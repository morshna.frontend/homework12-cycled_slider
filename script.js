const images = document.querySelectorAll(".image-to-show");
let current = 0;
const play = document.querySelector(".play");
const stop = document.querySelector(".stop");
let timer;

function slideShow() {
    for( let i = 0; i < images.length; i++){
        images[i].classList.add('opacity0')
    }
    images[current].classList.remove('opacity0');
    
    if(current+1 == images.length){
        current = 0;
    } else{
        current++;
    }
    timer = setTimeout("slideShow()", 10000);
}
window.onload = setTimeout("slideShow()", 10000);

stop.addEventListener('click', function() {
    clearTimeout(timer);
});
play.addEventListener('click', function() {
    timer = setTimeout("slideShow()", 10000);
})
